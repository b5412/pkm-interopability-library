const fs = require("fs");
const path = require("path");

class WikiToMd {
  /**
   * Replaces all wikilinks in a given file with markdown links.
   * @param {string} filePath - The path to the file to update.
   * @returns None
   */
  #replaceLinks(filePath) {
    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error(`Error reading file: ${filePath}`, err);
        return;
      }

      const replacedData = data.replace(
        /\[\[([^\]]+)\]\]|\[\[([^\]]+)\|([^\]]+)\]\]/g,
        (match, p1, p2, p3) => {
          if (p1) {
            return `[${p1}](${p1}.md)`;
          } else {
            return `[${p3}](${p2}.md)`;
          }
        }
      );

      fs.writeFile(filePath, replacedData, "utf8", (err) => {
        if (err) {
          console.error(`Error writing to file: ${filePath}`, err);
          return;
        }
        console.log(`File updated: ${filePath}`);
      });
    });
  }

  /**
   * Recursively traverses a directory and replaces all links in a given file from one link format to another format (e.g. wikilinks to markdown links).
   * @param {DirectoryPath} dirPath - The path of the directory to traverse.
   * @param {string} from - The file extension to replace.
   * @param {string} to - The new file extension to use.
   * @returns None
   */
  traverse(dirPath) {
    fs.readdir(dirPath, (err, files) => {
      if (err) {
        console.error(`Error reading directory: ${dirPath}`, err);
        return;
      }

      files.forEach((file) => {
        const filePath = path.join(dirPath, file);
        try {
          const stats = fs.statSync(filePath);
          if (stats.isDirectory()) {
            this.traverse(filePath);
          } else if (path.extname(filePath) === ".md") {
            this.#replaceLinks(filePath);
          }
        } catch (err) {
          console.error(`Error getting file stat: ${filePath}`, err);
        }
      });
    });
  }
}

class MarkdownToWiki {
  #replaceLinks(filePath) {
    fs.readFile(filePath, "utf8", function (err, data) {
      if (err) {
        console.error(`Error reading file: ${filePath}`, err);
        return;
      }

      const replacedData = data.replace(
        /(?<=\s)\[([^\]]+)\]\(([^\)]+)\.md\)/g,
        `[[$2|$1]]`
      );

      fs.writeFile(filePath, replacedData, "utf8", function (err) {
        if (err) {
          console.error(`Error writing to file: ${filePath}`, err);
          return;
        }
        console.log(`File updated: ${filePath}`);
      });
    });
  }

  /**
   * Recursively traverses a directory and replaces all links with the given file extension
   * with links to files with the new extension.
   * @param {DirectoryPath} dirPath - The path of the directory to traverse.
   * @param {string} from - The file extension to replace.
   * @param {string} to - The new file extension to use.
   * @returns None
   */
  traverse(dirPath) {
    fs.readdir(dirPath, (err, files) => {
      if (err) {
        console.error(`Error reading directory: ${dirPath}`, err);

        return;
      }

      files.forEach((file) => {
        const filePath = path.join(dirPath, file);

        fs.stat(filePath, (err, stat) => {
          if (err) {
            console.error(`Error getting file stat: ${filePath}`, err);

            return;
          }

          if (stat.isDirectory()) {
            this.traverse(filePath);
          } else if (path.extname(filePath) === ".md") {
            this.#replaceLinks(filePath);
          }
        });
      });
    });
  }
}

class AliasFormatter {
  replaceInFile(filePath) {
    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error(`Error reading file: ${filePath}`, err);

        return;
      }

      const replacedData = data.replace(/\[\[(.*?)\|(.*?)\]\]/g, "[[$2|$1]]");

      fs.writeFile(filePath, replacedData, "utf8", (err) => {
        if (err) {
          console.error(`Error writing to file: ${filePath}`, err);

          return;
        }

        console.log(`File updated: ${filePath}`);
      });
    });
  }

  traverseDir(dirPath) {
    fs.readdir(dirPath, (err, files) => {
      if (err) {
        console.error(`Error reading directory: ${dirPath}`, err);

        return;
      }

      files.forEach((file) => {
        const filePath = path.join(dirPath, file);

        fs.stat(filePath, (err, stat) => {
          if (err) {
            console.error(`Error getting file stat: ${filePath}`, err);

            return;
          }

          if (stat.isDirectory()) {
            this.traverseDir(filePath);
          } else if (path.extname(filePath) === ".md") {
            this.replaceInFile(filePath);
          }
        });
      });
    });
  }
}

/** @class RenmoveAlias
 * @description This class is used to remove the alias from the file name
 * @method #replaceLinks
 * @method traverse
 * @method removeAlias
 *
 * 
 * @summary -
 * 1. The #replaceLinks function takes as an argument the file path to the file being processed.
 * 2. The fs.readFile function is called to read the file content into the data variable.
 * 3. The data variable is searched for all links in the format [[link text|file name]] and the link text is replaced with the file name.
 * 4. The fs.writeFile function is called to write the modified data into the file.
 * 5. The traverse function recursively traverses the directory and calls the #replaceLinks function for each file in the directory.
 * 6. The traverse function takes as an argument the directory path to be traversed.
 * 7. The fs.readdir function is called to read the directory and get the list of files.
 * 8. The forEach function is called on the files variable to loop through each file in the directory.
 * 9. The path.join function is used to create the file path to the file being processed.
 * 10. The fs.statSync function is called to get the file stats and check if the file is a directory.
 * 11. If the file is a directory, the traverse function is called recursively to traverse the directory.
 * 12. If the file is a file, the #replaceLinks function is called to process the file.
 *
 */
class RemoveAlias {
  #replaceLinks(filePath) {
    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error(`Error reading file: ${filePath}`, err);
        return;
      }

      const replacedData = data.replace(
        /\[\[([^\]]+)\|([^\]]+)\]\]/g,
        (match, p1, p2) => `[[${p2}]]`
      );
      fs.writeFile(filePath, replacedData, "utf8", (err) => {
        if (err) {
          console.error(`Error writing to file: ${filePath}`, err);
          return;
        }
        console.log(`File updated: ${filePath}`);
      });
    });
  }

  /**
   * Recursively traverses a directory and replaces all links with the given file extension
   * with links to files with the new extension.
   * @param {DirectoryPath} dirPath - The path of the directory to traverse.
   * @param {string} from - The file extension to replace.
   * @param {string} to - The new file extension to use.
   * @returns None
   */
  traverse(dirPath) {
    fs.readdir(dirPath, (err, files) => {
      if (err) {
        console.error(`Error reading directory: ${dirPath}`, err);
        return;
      }

      files.forEach((file) => {
        const filePath = path.join(dirPath, file);
        try {
          const stats = fs.statSync(filePath);
          if (stats.isDirectory()) {
            this.traverse(filePath);
          } else if (path.extname(filePath) === ".md") {
            this.#replaceLinks(filePath);
          }
        } catch (err) {
          console.error(`Error getting file stat: ${filePath}`, err);
        }
      });
    });
  }
  removeAlias(filePath, linkText) {
    const extname = path.extname(filePath);
    const basename = path.basename(filePath, extname);
    if (basename === linkText) {
      return filePath.replace(`|${linkText}${extname}`, extname);
    } else {
      return filePath;
    }
  }
}

/**
 * A class that provides methods for converting between wiki and markdown formats, formatting aliases,
 * and removing aliases.
 * @param {string} rootDir - The root directory to traverse.
 */
class Main {
  constructor(rootDir) {
    this.rootDir = rootDir;
  }

  convertWikiToMd() {
    const convertWikiToMd = new WikiToMd();
    return convertWikiToMd.traverse(this.rootDir);
  }

  convertMarkdownToWiki() {
    const convertMarkdownToWiki = new MarkdownToWiki();
    return convertMarkdownToWiki.traverse(this.rootDir);
  }

  formatAliases() {
    const aliasFormatter = new AliasFormatter();
    return aliasFormatter.traverseDir(this.rootDir);
  }

  removeAlias() {
    const removeAlias = new RemoveAlias();
    return removeAlias.traverse(this.rootDir);
  }

  runOne(methodName) {
    switch (methodName) {
      case "convertWikiToMd":
        this.convertWikiToMd();
        break;
      case "convertMarkdownToWiki":
        this.convertMarkdownToWiki();
        break;
      case "formatAliases":
        this.formatAliases();
        break;
      case "removeAlias":
        this.removeAlias();
        break;
      default:
        console.log("No method found");
    }
  }

  runMany(methodNames = []) {
    if (!methodNames.length) {
      return;
    } else if (methodNames.every((el) => typeof el === String)) {
      return;
    } else {
      methodNames.forEach((methodName) => {
        console.log(typeof methodName);
        this.runOne(methodName);
      });
    }
  }
}

const rootDir = "/Users/christopherharwell/Documents/temp";
const main = new Main(rootDir);

main.runOne("removeAlias");
// main.runOne("convertMarkdownToWiki");
// main.runMany(["removeAlias", "convertWikiToMd"]);
